import os
import logging
from fastapi import FastAPI, Request, HTTPException
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from scripts.session import is_redis_working
from scripts.sfapi import get_availability
from scripts.utils import extract_livechat_config
from scripts.message_handler import handle_message, handle_event

# Configuration
ENVIRONMENT = os.getenv("ENVIRONMENT")
ELASTIC_APM_SERVER_URL = os.getenv("ELASTIC_APM_SERVER_URL")
ELASTIC_APM_SECRET_TOKEN = os.getenv("ELASTIC_APM_SECRET_TOKEN")
SALESFORCE_ADAPTER_SENTRY_DSN = os.getenv("SALESFORCE_ADAPTER_SENTRY_DSN")

app = FastAPI()

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
)

logger = logging.getLogger("main")

# Configure Sentry for error monitoring if DSN is available
if SALESFORCE_ADAPTER_SENTRY_DSN:
    import sentry_sdk

    sentry_sdk.init(dsn=SALESFORCE_ADAPTER_SENTRY_DSN)

# Configure Elastic APM if server URL is available
if ELASTIC_APM_SERVER_URL:
    apm = make_apm_client({
        "ELASTIC_APM_SERVER_URL": ELASTIC_APM_SERVER_URL,
        "ELASTIC_APM_SECRET_TOKEN": ELASTIC_APM_SECRET_TOKEN,
        "SERVICE_NAME": f"Gem Camunda Cache {ENVIRONMENT}"
    })
    app.add_middleware(ElasticAPM, client=apm)

@app.on_event("startup")
async def startup_event():
    """Check if Redis is working on startup"""
    if not is_redis_working():
        raise RuntimeError("Redis is not working. Please check your Redis configuration.")

@app.get('/')
async def index():
    """Check if the service is up and running"""
    return {'status': 'ok'}

@app.get('/{livechat_config}/livechat/')
def livechat(request: Request):
    """
    Check if the livechat is available
    params:
        livechat_config: the livechat configuration
    returns:
        a dict with the status of the livechat
    example return values:
    {"status": "ok"} # medewerkers beschikbaar gedurende de openingsuren V
    {"status": "not_available"} # medewerkers niet beschikbaar gedurende de openingsuren X
    {"status": "closed"} # buiten de openingsuren X
    {"status": "not_ok"} # livechat is gesloten of bezet V
    """
    livechat_config = extract_livechat_config(request)
    availability = get_availability(livechat_config)

    if availability.get("success") is True:
        return {'status': 'ok'}
    else:
        return {'status': 'not_ok'}

@app.post('/{livechat_config}/livechat/')
async def livechat_post(request: Request):
    """
    This function handles incoming messages from the Gem chatbot.
    Args:
        request (Request): The request
    Returns:
        None
    """
    try:
        data = await request.json()
        sender = data.get('sender')
        livechat_config = extract_livechat_config(request)

        if 'message' in data:
            await handle_message(data, sender, livechat_config)

        elif 'event' in data:
            await handle_event(data, sender)

        else:
            raise HTTPException(status_code=400, detail=f'Invalid data for session "{sender}"')
    except Exception as error:
        raise HTTPException(status_code=500, detail=str(error)) from error