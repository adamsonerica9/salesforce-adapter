# from official uvicorn-gunicorn-fastapi image
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11

# set working directory
WORKDIR /app

# Copy requirements.txt to working directory
COPY requirements.txt .

# install dependencies without cache
RUN pip install -r requirements.txt --no-cache-dir

# copy scripts folder to working directory
COPY ./scripts ./scripts

# copy main.py into working directory
COPY ./main.py .
