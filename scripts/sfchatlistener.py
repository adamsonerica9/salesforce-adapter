import asyncio
import logging
import json
from scripts.sfapi import pull_messages
from scripts.gemapi import (
    post_agent_joined,
    post_text_message,
    post_button_message,
    pass_chat_to_gem,
    set_typing_on,
    set_typing_off,
    send_system_message
    )

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
)

logger = logging.getLogger("sfchatlistener")

IDLE_TIME = 10 * 60

async def start_chat_listener(recipient_id, affinity, session_key, db_client):
    """
    This function listens for Salesforce chat events and sends them to the Gem chatbot.
    Args:
        recipient_id (str): The recipient_id
        affinity: The affinity
        session_key: The session key
    Returns:
        None
    """
    session_data = db_client.get(recipient_id)
    session_data = json.loads(session_data)
    livechat_config = session_data["livechat_config"]

    # Idle timer
    timer = None

    async def idle_timer(recipient_id):
        """
        This function is called when the idle timer expires.
        Args:
            recipient_id (str): The recipient_id of the Salesforce chat session
        Returns:
            None
        """
        await asyncio.sleep(IDLE_TIME)
        # temp workaround: send typing_on event to trigger "Je gaat nu chatten met een medewerker"
        # temp workaround: sleep for 1 second to make sure the typing event is sent before the first message
        # needs to be fixed in the Router
        await set_typing_on(recipient_id)
        await asyncio.sleep(1)
        await pass_chat_to_gem(recipient_id, "error", "timeout")
        nonlocal continue_polling
        continue_polling = False

    async def start_timer():
        """
        This function starts the idle timer.
        Args:
            None
        Returns:
            None
        """
        nonlocal timer
        timer = asyncio.create_task(idle_timer(recipient_id))

    async def restart_timer():
        """
        This function restarts the idle timer.
        Args:
            None
        Returns:
            None
        """
        nonlocal timer
        if timer:
            timer.cancel()
        await start_timer()

    continue_polling = True

    # Start long polling
    while continue_polling:
        try:
            result = await pull_messages(affinity, session_key, livechat_config)
            logger.info("result: %s for user %s", result, recipient_id)

            if result and result["messages"] and len(result["messages"]) > 0:
                message = result["messages"][0]["message"]
                message_type = result["messages"][0]["type"]

                if message_type == "ChatRequestSuccess":
                    logger.info("> Waiting for Salesforce agent to accept your chat request.")

                elif message_type == "ChatRequestFail":
                    logger.warning("> Failed to route Salesforce chat to agent with reason \"%s\".", message['reason'])
                    # temp workaround: send typing_on event to trigger "Je gaat nu chatten met een medewerker"
                    # temp workaround: sleep for 1 second to make sure the typing event is sent before the first message
                    # needs to be fixed in the Router
                    await set_typing_on(recipient_id)
                    await asyncio.sleep(1)
                    
                    if message['reason'] == 'Unavailable':
                        logger.info("> No agents available. Returning to Gem.")
                        await pass_chat_to_gem(recipient_id, "not_available", message.get('reason'))
                    else:
                        await pass_chat_to_gem(recipient_id, "error", message.get('reason'))
                        logger.info("> Error in chat request. Returning to Gem.")
                    
                    continue_polling = False

                elif message_type == "ChatEstablished":
                    logger.info("> Salesforce agent accepted chat request. Listening for messages ...")
                    await post_agent_joined(recipient_id)
                    # temp workaround: send typing_on event to trigger "Je gaat nu chatten met een medewerker"
                    # temp workaround: sleep for 1 second to make sure the typing event is sent before the first message
                    # needs to be fixed in the Router
                    await set_typing_on(recipient_id)
                    await asyncio.sleep(1)
                    
                    # Start idle timer
                    await start_timer()

                elif message_type in ["ChatMessage", "RichMessage"]:
                    if message_type == "RichMessage" and "items" in message and message["items"]:
                        message_subtype = message["type"]
                        if message_subtype in ["ChatWindowMenu", "ChatWindowButton"]:
                            buttons = [{"title": item["text"], "payload": item["text"]} for item in message["items"]]
                            await post_button_message('Maak uw keuze.', buttons, recipient_id)
                    else:
                        logger.info("> Salesforce agent send message")
                        await post_text_message(message["text"].replace('RICH_TEXT:', '').replace('/<\/?[^>]+(>|$)/g', ''), recipient_id)

                    # Restart idle timer
                    await restart_timer()

                elif message_type == "ChatEnded" or message_type == "AgentDisconnect":
                    logger.info("> Salesforce agent has ended the chat.")
                    await pass_chat_to_gem(recipient_id, "success")
                    continue_polling = False
                
                elif message_type == "ChatTransferred":
                    logger.info("> Salesforce agent has transferred the chat.")
                    #TODO: moet gem dit opvangen?
                    await post_text_message('U wordt doorverbonden met een andere medewerker.', recipient_id)

                elif message_type == "AgentTyping":
                    logger.info("> Salesforce agent started typing.")
                    await set_typing_on(recipient_id)

                elif message_type == "AgentNotTyping":
                    logger.info("> Salesforce agent stopped typing.")
                    await set_typing_off(recipient_id)

                elif message_type == "QueueUpdate":
                    logger.info("> The queue position has changed for session %s", recipient_id)
                    position = message.get('position')
                    if position == 0:
                        await send_system_message("livechat.queue.busy", recipient_id)
                    else:
                        # send a reminder that the user is in the queue
                        await send_system_message("livechat.queue.busy_position", recipient_id, position=position)

                else:
                    logger.warning("Unhandled Salesforce chat event %s", result)

            else:
                session_exists = db_client.get(recipient_id)
                if session_exists:
                    logger.info("> Waiting for Salesforce agent to send a message ...")
                else:
                    logger.info("> This session has been closed.")
                    continue_polling = False


        except Exception as error:
            logger.error("Something went wrong. %s", error)
            continue_polling = False

    # End process
    logger.info("End Salesforce chat listener for user %s", recipient_id)