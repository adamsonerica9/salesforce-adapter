import os
import time
import redis
import logging

logging.basicConfig(level=logging.INFO)

redis_host = os.environ.get("REDIS_HOST", "redis")
redis_port = int(os.environ.get("REDIS_PORT", 6379))
redis_db = int(os.environ.get("REDIS_DB", 12))

def is_redis_working(retry_attempts=5, retry_sleep=2):
    """
    Check if Redis is working by performing a simple SET/GET operation.
    Args:
        retry_attempts (int): Number of retry attempts in case of failure (default: 5).
        retry_sleep (int): Sleep time in seconds between retry attempts (default: 2).
    Returns:
        bool: True if Redis is working, False otherwise.
    """
    try:
        redis_client = redis.StrictRedis(
            host=redis_host,
            port=redis_port,
            db=redis_db,
            decode_responses=True
            )
        
        test_key = "12345"
        test_value = "56789"

        for attempt in range(retry_attempts + 1):
            # Try setting the test key
            redis_client.set(test_key, test_value)
            # Try getting the test key
            get_key = str(redis_client.get(test_key))
            # Check if the key is successfully set
            if get_key == test_value:
                # Delete the test key after the successful test
                redis_client.delete(test_key)
                return True
            
            # Sleep before the next retry attempt
            time.sleep(retry_sleep)

            if attempt < retry_attempts:
                logging.warning("Attempt %s/%s: Redis is not responding. Retrying in %s seconds...", attempt, retry_attempts, retry_sleep)

        return False

    except Exception as error:
        logging.error("Error while checking if Redis is working: %s", error)
        return False
    
def get_redis_client():
    """
    This function returns a Redis client
    Args:
        None
    Returns:
        Redis client
    """
    return redis.StrictRedis(
        host=redis_host,
        port=redis_port,
        db=redis_db,
        decode_responses=True
        )