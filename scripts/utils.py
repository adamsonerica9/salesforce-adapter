import base64
from fastapi import HTTPException

import logging

logging.basicConfig(level=logging.INFO)

def extract_livechat_config(request):
    try:
        # extract url params
        livechat_config = request.path_params['livechat_config']

        if livechat_config:
            livechat_config = livechat_config.split('-')
            livechat_config = {
                'SF_SERVER_URL': base64.b64decode(livechat_config[0]).decode("utf-8"),
                'CHAT_BUTTONID': livechat_config[1],
                'CHAT_DEPLOYMENTID': livechat_config[2],
                'CHAT_ORGANISATIONID': livechat_config[3]
            }
            return livechat_config
        else:
            logging.error(f'Error extracting livechat_config: {str(error)}')
            raise HTTPException(
                status_code=400,
                detail='No livechat_config found in URL'
            )
    except Exception as error:
        logging.error(f'Error extracting livechat_config: {str(error)}')
        raise HTTPException(
            status_code=400,
            detail=f'Error extracting livechat_config: {str(error)}'
        ) from error