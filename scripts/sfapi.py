from typing import Optional
import logging
import aiohttp
import requests

from scripts.endpoints import (
    SESSION_ID,
    CHAT_REQUEST,
    PULL_MESSAGES,
    SEND_MESSAGES,
    STOP_CHAT,
    AVAILABILITY
    )

logging.basicConfig(level=logging.INFO)

API_VERSION = "54"

def get_availability(livechat_config) -> dict:
    """
    This function gets the availability of the Salesforce chat service.
    args:
        None
    returns:
        a dict with the success status and the data from the Salesforce chat API
    """
    headers = {
        "X-LIVEAGENT-API-VERSION": API_VERSION,
    }
    params = {
        "org_id": livechat_config.get("CHAT_ORGANISATIONID"),
        "deployment_id": livechat_config.get("CHAT_DEPLOYMENTID"),
        "Availability.ids" : livechat_config.get("CHAT_BUTTONID"),
        "Availability.needEstimatedWaitTime": 1 # Specify a value of 1 to request the estimated wait time
    }

    try:
        response = requests.get(
            f"{livechat_config.get('SF_SERVER_URL')}{AVAILABILITY}",
            headers=headers,
            params=params,
            timeout=10
        )
        if response.status_code == 200:
            data = response.json()
            is_available = data.get("messages", [{}])[0].get("message", {}).get("results", [{}])[0].get("isAvailable", False)
            return {"success": is_available}
        else:
            logging.info("response.status_code: %d (%s)", response.status_code, response.reason)
            return {"success": False}
    except Exception as error:
        logging.error("error in get_availability: %s", error)
        return {"success": False}
    
async def new_chat_session(livechat_config) -> dict:
    """
    This function requests a new Salesforce chat session.
    args:
        None
    returns:
        a dict with the success status and the data from the Salesforce chat API
    """
    headers = {
        "X-LIVEAGENT-API-VERSION": API_VERSION,
        "X-LIVEAGENT-AFFINITY": "null"
    }
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{livechat_config.get('SF_SERVER_URL')}{SESSION_ID}",
                headers=headers
                ) as response:
                data = await response.json()
                return {
                    "success": True,
                    "data": data
                }
    except Exception as error:
        logging.error("error in new_chat_session: %s", error)
        return {
            "success": False
        }

async def init_chat(
        affinity: str,
        sessionkey: str,
        sessionid: str,
        metadata: Optional[dict],
        livechat_config: dict
        ) -> bool:
    """
    This function initializes the Salesforce chat session.
    args:
        affinity: the affinity token of the Salesforce chat session
        sessionkey: the session key of the Salesforce chat session
        sessionid: the session id of the Salesforce chat session
        metadata: the metadata of the Salesforce chat session
    returns:
        True if the chat session was initialized successfully, False otherwise
    """
    body = {
        "organizationId": livechat_config.get("CHAT_ORGANISATIONID"),
        "deploymentId": livechat_config.get("CHAT_DEPLOYMENTID"),
        "buttonId": livechat_config.get("CHAT_BUTTONID"),
        "sessionId": sessionid,
        "userAgent": "Lynx/2.8.8",
        "language": "nl-NL",
        "screenResolution": "1900x1080",
        "visitorName": "Gebruiker via Gem",
        "prechatDetails": [
            {
                "label": "Onderwerp",
                "value": "Tilburg GEM",
                "transcriptFields": [
                    "Subject"
                ],
                "displayToAgent": True
            },
            {
                "label": "Gesprek",
                "value": metadata.get("chatHistoryPlain") if metadata else None,
                "transcriptFields": [
                    "External_Chat_Transcript__c"
                ],
                "displayToAgent": True
            },
            {
                "label": "History",
                "value": metadata.get("chatHistory") if metadata else None,
                "entityMaps": [
                    {
                        "entityName": "ChatTranscript",
                        "fieldName": "External_Chat_Transcript__c"
                    }
                ],
                "transcriptFields": [
                    "External_Chat_Transcript__c"
                ],
                "displayToAgent": False
            },
            {
                "label": "Has Chat History?",
                "value": True,
                "entityMaps": [
                    {
                        "entityName": "ChatTranscript",
                        "fieldName": "Has_External_Chat_History__c"
                    }
                ],
                "transcriptFields": [
                    "Has_External_Chat_History__c"
                ],
                "displayToAgent": False
            }
        ],
        "prechatEntities": [],
        "receiveQueueUpdates": True,
        "isPost": True
    }

    headers = {
        "X-LIVEAGENT-API-VERSION": API_VERSION,
        "X-LIVEAGENT-AFFINITY": affinity,
        "X-LIVEAGENT-SESSION-KEY": sessionkey,
        "X-LIVEAGENT-SEQUENCE": "1"
    }
    
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                f"{livechat_config.get('SF_SERVER_URL')}{CHAT_REQUEST}",
                json=body,
                headers=headers
                ) as response:
                if response.status == 200:
                    return True
                else:
                    logging.info("response.status: %d (%s)", response.status, response.reason)
                    return False

    except Exception as error:
        logging.error("error in init_chat: %s", error)
        return False

async def pull_messages(affinity: str, sessionkey: str, livechat_config: dict) -> dict:
    """
    This function gets the messages from the Salesforce chat session.
    args:
        affinity: the affinity token of the Salesforce chat session
        sessionkey: the session key of the Salesforce chat session
    returns:
        the response from the Salesforce chat API
    """
    headers = {
        "X-LIVEAGENT-API-VERSION": str(API_VERSION),
        "X-LIVEAGENT-AFFINITY": str(affinity),
        "X-LIVEAGENT-SESSION-KEY": str(sessionkey)
    }
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{livechat_config.get('SF_SERVER_URL')}{PULL_MESSAGES}",
                timeout=10,
                headers=headers
                ) as response:
                data = await response.json()
                return data
    except Exception as error:
        logging.error("error in pull_messages: %s", error)
        return None

async def send_message(text: str, affinity: str, sessionkey: str, livechat_config) -> dict:
    """
    This function sends a message to the Salesforce chat session.
    args:
        text: the text to send
        affinity: the affinity token of the Salesforce chat session
        sessionkey: the session key of the Salesforce chat session
    returns:
        response_text: the text of the response from the Salesforce chat API
    """
    payload = {"text": text}

    headers = {
        "X-LIVEAGENT-API-VERSION": API_VERSION,
        "X-LIVEAGENT-AFFINITY": affinity,
        "X-LIVEAGENT-SESSION-KEY": sessionkey
    }
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                f"{livechat_config.get('SF_SERVER_URL')}{SEND_MESSAGES}",
                json=payload,
                headers=headers
                ) as response:
                response_text = await response.text()
                return response_text
            
    except Exception as error:
        logging.error("error in send_message: %s", error)
        return None

async def stop_chat(reason: str, affinity: str, sessionkey: str, livechat_config) -> dict:
    """
    This function stops the Salesforce chat session.
    Args:
        reason (str): the reason to stop the chat session
        affinity (str): the affinity token of the Salesforce chat session
        sessionkey (str): the session key of the Salesforce chat session
    Returns:
        response_text: the text of the response from the Salesforce chat API
    """
    payload = {"reason": "client"}
    headers = {
        "X-LIVEAGENT-API-VERSION": API_VERSION,
        "X-LIVEAGENT-AFFINITY": affinity,
        "X-LIVEAGENT-SESSION-KEY": sessionkey,
    }
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(
                f"{livechat_config.get('SF_SERVER_URL')}{STOP_CHAT}",
                json=payload,
                headers=headers
            ) as response:
                response_text = await response.text()
                return response_text
            
    except Exception as error:
        logging.error("Error in stop_chat: %s", error)
        return None