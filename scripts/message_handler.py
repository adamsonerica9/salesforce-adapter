import json
import logging
import asyncio
from fastapi import HTTPException
from scripts import sfchatlistener
from scripts.sfapi import new_chat_session, init_chat, send_message, stop_chat
from scripts.gemapi import send_system_message
from scripts.session import get_redis_client

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
)

logger = logging.getLogger("message_handler")

db_client = get_redis_client()
chat_listener_tasks = {} #TODO: use a better way to store the chat listener tasks

    
async def handle_message(data: dict, sender: str, livechat_config: dict) -> None:
    """
    This function handles incoming messages from the Gem chatbot.
    Args:
        data (dict): The message data
        sender (str): The sender
        livechat_config (str): The livechat configuration
    Returns:
        None
    """
    message = data.get('message')

    if message.startswith('/init'):
        await send_system_message("livechat.queue.welcome", sender)
        await initialize_chat(message, sender, livechat_config)

    else:
        await send_gem_message(message, sender)

async def initialize_chat(message: str, sender: str, livechat_config: dict) -> None:
    """
    This function initializes a Salesforce chat session.
    Args:
        message (str): The message
        sender (str): The sender
        livechat_config (str): The livechat configuration
    Returns:
        None
    """
    message_data = message.replace('/init', '')
    chat_history = json.loads(message_data).get('chat_history', [])
    chat_history_plain = json.loads(message_data).get('chat_history_plain', '\n' + 'geen chatgeschiedenis gevonden')

    sf_chat_session = await new_chat_session(livechat_config)

    if sf_chat_session["success"]:
        session_key = sf_chat_session["data"]["key"]
        affinity = sf_chat_session["data"]["affinityToken"]
        session_id = sf_chat_session["data"]["id"]

        metadata = {
            "chatHistory": json.dumps(chat_history),
            "chatHistoryPlain": chat_history_plain
        }

        chat_initialized = await init_chat(
            affinity,
            session_key,
            session_id,
            metadata,
            livechat_config
        )

        if chat_initialized:
            logger.info("Chat initialized: %s for session %s", chat_initialized, sender)
            db_client.set(
                sender,
                json.dumps({
                    'affinity': affinity,
                    'sessionKey': session_key,
                    'livechat_config': livechat_config,
                })
            )

            chat_listener_task = asyncio.create_task(
                sfchatlistener.start_chat_listener(
                    sender,
                    affinity,
                    session_key,
                    db_client
                )
            )

            chat_listener_tasks[sender] = chat_listener_task
            return 'success'

        else:
            raise HTTPException(
                status_code=400,
                detail=f'Chat initialization failed for session "{sender}"'
            )
    else:
        raise HTTPException(
            status_code=400,
            detail=f'Chat initialization failed for session "{sender}"'
        )


async def send_gem_message(message: str, sender: str) -> None:
    """
    This function sends a message to Salesforce.
    Args:
        message (str): The message
        sender (str): The sender
    Returns:
        None
    """
    session_data = db_client.get(sender)
    if session_data:
        session_info = json.loads(session_data)
        await send_message(
            message,
            session_info['affinity'],
            session_info['sessionKey'],
            session_info['livechat_config']
        )
    else:
        raise HTTPException(
            status_code=400,
            detail=f'No task found for session "{sender}"'
        )


async def handle_event(data: dict, sender: str) -> None:
    """
    This function handles incoming events from the Gem chatbot.
    Args:
        data (dict): The event data
        sender (str): The sender
    Returns:
        None
    """
    event = data.get('event')

    if event == 'user_disconnected':
        session_data = db_client.get(sender)
        if session_data:
            session_info = json.loads(session_data)
            await send_message(
                "De gebruiker heeft de chat verlaten.",
                session_info['affinity'],
                session_info['sessionKey'],
                session_info['livechat_config']
            )
            await stop_chat(
                "De gebruiker heeft de chat verlaten.",
                session_info['affinity'],
                session_info['sessionKey'],
                session_info['livechat_config']
            )

            db_client.delete(sender)

            chat_listener_task = chat_listener_tasks.get(sender)
            if chat_listener_task:
                chat_listener_task.cancel()
                logger.info("Chat listener task cancelled for session %s", sender)
                del chat_listener_tasks[sender]
        else:
            raise HTTPException(
                status_code=400,
                detail=f'No thread found for session "{sender}"'
            )