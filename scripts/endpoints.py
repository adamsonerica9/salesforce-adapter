# REST API Endpoints
CHAT_REQUEST = "Chasitor/ChasitorInit"
PULL_MESSAGES = "System/Messages"
SEND_MESSAGES = "Chasitor/ChatMessage"
STOP_CHAT = "Chasitor/ChatEnd"
SESSION_ID = "System/SessionId"
AVAILABILITY = "Visitor/Availability"
