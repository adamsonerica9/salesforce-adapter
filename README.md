# Salesforce-adapter

This is a Salesforce-adapter that can be run using Docker Compose.

## Requirements

- Docker
- Docker Compose

## Environment Variables

Before running the application, make sure to create a `.env` file in the root directory of your project and add the following environment variables:

```plaintext
ROUTER_URL=   # Replace with the URL for the router
ROUTER_API_KEY=   # Replace with the key for the router
```

## Usage

### Development Mode

To build the Salesforce image, use the following command:

```bash
sudo docker-compose -f docker-compose-dev.yml build
```

To run the application in development mode with debugging and auto-reloading enabled, use the following command:

```bash
docker-compose -f docker-compose-dev.yml up
```

This will start the FastAPI application on port 5000, and any changes to the code will automatically trigger a reload.

### Production mode

To run the application in production mode with debugging and auto-reloading disabled, use the following command:

```bash
docker-compose -f docker-compose-prod.yml up
```

This will start the FastAPI application on port 5000, optimized for production use.

#### Configuration
Both docker-compose-dev.yml and docker-compose-prod.yml files allow you to configure certain environment variables for the application:

- DEBUG: Set to True in development mode to enable debugging features. Set to False in production mode to disable debugging.
- RELOAD: Set to True in development mode to enable auto-reloading when code changes. Set to False in production mode to disable auto-reloading.

Modify these environment variables based on your specific requirements.

## TO DO
- [x] Handle user_disconnect events. We are not receiving a "user_disconnect" event after disconnecting.
- [x] Find a better way to handle sessions: Currently, sessions are saved in a dictionary, and they are removed from this dictionary upon disconnect. The dictionary is stored in memory. Should we consider handling sessions via Redis for better management?
- [x] Manage sessions for polling: Destroy polling threads upon disconnect or inactivity.
- [ ] Manage asyncio tasks via Redis instead of in memory dictionary
- [ ] 